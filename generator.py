from fpdf import FPDF

#Tamaño de imagen: 564 X 770
print("Ingrese opcion:")
print("    1: Español")
print("    2: Ingles")
nb0 = input()
if nb0 == '1':
	mypath="images-esp/"
	lang="es"
if nb0 == '2':
	mypath="images/"
	lang="en"
	


from os import listdir
from os.path import isfile, join
onlyfiles = [f for f in sorted(listdir(mypath)) if isfile(join(mypath, f))]
c=0

print("Ingrese opcion:")
print("    1: A4")
print("    2: A3")
nb = input()
if nb == '1':
	posiciones=[[4 ,8],[73 ,8],[142 ,8],[4 ,103],[73 ,103],[142 ,103],[4 ,198],[73 ,198],[142 ,198]]
	cartasxhoja=9
	formato="A4"
	print ('A4 elegido')
	pdf = FPDF(orientation = 'P', format='A4')

else:
	posiciones=[[4 ,8],[73 ,8],[142 ,8],[211 ,8],[280 ,8],[349 ,8],[4 ,103],[73 ,103],[142 ,103],[211 ,103],[280 ,103],[349 ,103],[4 ,198],[73 ,198],[142 ,198],[211 ,198],[280 ,198],[349 ,198]]
	cartasxhoja=18
	formato="A3"
	print ('A3 elegido')
	pdf = FPDF(orientation = 'L', format='A3')



for imagen in onlyfiles:	
	#print ("debo añadir imagen: "+imagen)
	# print ("archivo numero: "+str(c))
	# print ("pagina:"+str(c//cartasxhoja))
	# print ("posicion:"+str(c%cartasxhoja))
	# print(posiciones[c%cartasxhoja])
	print("Añadiendo imagen: "+mypath+imagen)
	if c%cartasxhoja == 0:
		pdf.add_page()
	pdf.image(mypath+imagen,posiciones[c%cartasxhoja][0] ,posiciones[c%cartasxhoja][1], 65 , 89)
	c=c+1

pdf.output("cartas-"+formato+"-"+lang+".pdf")
